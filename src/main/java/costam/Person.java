package costam;

public class Person {
        private String firstName;
        private String lastName ;

    Person (String _firstName,String _lastName)
    {
        firstName=_firstName;
        lastName=_lastName;
    }
    Person ()
    {
        lastName=null;
        firstName=null;
    }
    public void setFirstname(String firstname) {
        this.firstName = firstname;
    }
    public String getFirstname() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

}
